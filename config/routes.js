/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  //Common
  '/error': {
    controller: 'CommonController',
    action: 'errorPage'
  },
  '/success': {
    controller: 'CommonController',
    action: 'successPage'
  },





  //Authenticatin Routes
  '/login': {
    controller: 'AuthController',
    action: 'loginPage'
  },

  '/register': {
    controller: 'AuthController',
    action: 'registerPage'
  },

  '/logout': {
    controller: 'AuthController',
    action: 'logout'
  },

  'POST /auth/login': {
    controller: 'AuthController',
    action: 'loginProcess'
  },

  'POST /auth/loginSystem': {
    controller: 'AuthController',
    action: 'loginSystemProcess'
  },

  'POST /auth/register': {
    controller: 'AuthController',
    action: 'registerProcess'
  },






  '/': {
    controller: 'CommonController',
    action: 'homePage'
  },

  // Manager Routes
  'GET /manager/get-managers': {
    controller: 'ManagerController',
    action: 'getManagers'
  },

  '/manager/add-manager': {
    controller: 'ManagerController',
    action: 'addManager'
  },

  '/manager/edit-manager': {
    controller: 'ManagerController',
    action: 'editManager'
  },

  '/manager/remove-manager': {
    controller: 'ManagerController',
    action: 'removeManager'
  },

  'GET /manager/managers': {
    controller: 'ManagerController',
    action: 'managersPage'
  },

  'GET /manager/editManager': {
    controller: 'ManagerController',
    action: 'getManagerByID_forEdit'
  },

  'GET /manager/addManager': {
    controller: 'ManagerController',
    action: 'addManagerPage'
  },





  // Branch Routes
  'GET /branch/get-branches': {
    controller: 'BranchController',
    action: 'getBranches'
  },

  '/branch/add-branch': {
    controller: 'BranchController',
    action: 'addBranch'
  },

  '/branch/edit-branch': {
    controller: 'BranchController',
    action: 'editBranch'
  },

  '/branch/remove-branch': {
    controller: 'BranchController',
    action: 'removeBranch'
  },

  'GET /branch/branches': {
    controller: 'BranchController',
    action: 'branchesPage'
  },

  'GET /branch/editBranch': {
    controller: 'BranchController',
    action: 'getBranchByID_forEdit'
  },

  'GET /branch/addBranch': {
    controller: 'BranchController',
    action: 'addBranchPage'
  },





  // Cars Routes
  'GET /car/get-cars': {
    controller: 'CarController',
    action: 'getCars'
  },

  '/car/add-car': {
    controller: 'CarController',
    action: 'addCar'
  },

  '/car/edit-car': {
    controller: 'CarController',
    action: 'editCar'
  },

  '/car/remove-car': {
    controller: 'CarController',
    action: 'removeCar'
  },

  '/car/cars': {
    controller: 'CarController',
    action: 'carsPage'
  },

  '/car/addCar': {
    controller: 'CarController',
    action: 'addCarPage'
  },

  '/car/editCar': {
    controller: 'CarController',
    action: 'getCarByID_forEdit'
  },




  // Extras Routes
  'GET /extras/get-extras': {
    controller: 'ExtrasController',
    action: 'getExtras'
  },

  '/extras/add-extras': {
    controller: 'ExtrasController',
    action: 'addExtras'
  },

  '/extras/edit-extras': {
    controller: 'ExtrasController',
    action: 'editExtras'
  },

  '/extras/remove-extras': {
    controller: 'ExtrasController',
    action: 'removeExras'
  },

  'GET /extras/extras': {
    controller: 'ExtrasController',
    action: 'extrasPage'
  },

  '/extras/addExtras': {
    view: 'extras/addExtras'
  },

  'GET /extras/editExtras': {
    controller: 'ExtrasController',
    action: 'editExtrasPage'
  },




  // Notifications Routes
  'GET /notification/get-notifications': {
    controller: 'NotificationController',
    action: 'getNotifications'
  },

  '/notification/add-notification': {
    controller: 'NotificationController',
    action: 'addNotification'
  },

  '/notification/edit-notification': {
    controller: 'NotificationController',
    action: 'editNotification'
  },

  '/notification/remove-notification': {
    controller: 'NotificationController',
    action: 'removeNotification'
  },

  '/notification/notifications': {
    controller: 'NotificationController',
    action: 'notificationsPage'
  },

  '/notification/editNotification': {
    controller: 'NotificationController',
    action: 'getNotificationByID_forEdit'
  },

  'GET /notification/subscribe-notifications': {
    controller: 'NotificationController',
    action: 'subscribeToNotification'
  },

  '/notification/update-notifications': {
    controller: 'NotificationController',
    action: 'updateNoti'
  },


  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
