var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    jwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt ? require('passport-jwt').ExtractJwt : null,
    bycrypt = require('bcryptjs');


var EXPIRES_IN_MINUTES = 60 * 24
var SECRET = process.env.tokenSecret || '4ukI0uIVnB3iI1yxj646fVXSE3ZVk4doZgz6fTbNg7jO41EAtl20J5F7Trtwe7OM'
var ALGORITHM = 'HS256'
var ISSUER = 'app.com'
var AUDIENCE = 'app.com'

var LOCAL_STRATEGY_CONFIG = {
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: false
}
var jwtFromRequest = ExtractJwt ? ExtractJwt.fromAuthHeader() : null;
var JWT_STRATEGY_CONFIG = {
  jwtFromRequest: jwtFromRequest,
  secretOrKey: SECRET,
  issuer: ISSUER,
  audience: AUDIENCE,
  passReqToCallback: false
}

//passport.serializeUser and passport.deserializeUser setup a login session.
passport.serializeUser(function(user, done){
  done(null, user.username);
});

passport.deserializeUser(function(username, done){
  User.findOne({username: username}, function(err, user){
    if (err) done(err)
    if (user) {
      done(null, user)
    } else {
      Manager.findOne({username: username}, function(err, manager) {
        if (err) done(err)
        done(null, manager)
      })
    }
  });
});

function _onLocalStrategyAuth(username, password, next) {
  User.findOne({username:username}).exec(function(err, user) {
    if (err) {
      return next(err, false, {});
    }
    if (!user) {
      Manager.findOne({username: username}, function(err, manager) {
        if(err) {
          return next(err, false, {})
        }
        if (!manager) {
          return next(null, false, {
            code: 'E_MANAGER_NOT_FOUND',
            message: username + ' is not found'
          })
        }
        bycrypt.compare(password, manager.password, function(errManager, resManager) {
          if (errManager || !resManager) {
            return next(null, false, {
              code: 'E_WRONG_PASSWORD',
              message: 'Wrong password'
            });
          } else {
            return next(null,manager, {
              type: 'manager'
            });
          }
        })
      })
    } else {
      bycrypt.compare(password, user.password, function(err, res) {
      if (err || !res) {
        return next(null, false, {
          code: 'E_WRONG_PASSWORD',
          message: 'Wrong password'
        });
      } else {
        return next(null,user, {
          type: 'user'
        });
      }
    });
    }
  });
}

function _onJwtStrategyAuth(payload, next) {
  var user = payload.user
  return next(null, user, {})
}

passport.use(new LocalStrategy(LOCAL_STRATEGY_CONFIG, _onLocalStrategyAuth))
passport.use(new jwtStrategy(JWT_STRATEGY_CONFIG, _onJwtStrategyAuth))

// passport.use(new LocalStrategy({
//   usernameField: 'username',
//   passwordField: 'password'
// },
//   function(username, password, done){
//     User.findOne({username: username}).exec(function(err, user){
//       if (err){
//         return done(err);
//       }
//       if (!user){
//         return done(null, false, {
//           message: 'Unknown user ' + username
//         });
//       }
//       bycrypt.compare(password, user.password, function(err, res){
//         if(!res){
//           console.log('- Passport.Service error= ', err, ', res= ', res);
//           return done(null, false, {
//             message : 'Invalid password'
//           });
//         }
//         var returnUser = {
//           username: user.username,
//           role: user.role,
//           createdAt: user.createdAt,
//           id: user.id
//         }
//         return done(null, returnUser, {
//           message: 'Logged in successfully'
//         });
//       });
//     });
//   }
// ));
