/**
 * BranchController
 *
 * @description :: Server-side logic for managing branches
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

function removeOldManager(req, res, branch_id, manager_id, branch, op) {
	/* Check if there are any branches should be nulled */
	Manager.find({deleted: false}, function(err, managers) {
		if (err) {
			return res.view('common/error', {
				code: 'E_REM_OLD_MANAGER',
				message: res.i18n('E_REM_OLD_MANAGER'),
				details: err
			})
		}
    var managerFlag = false
		for (var i=0 ; i<managers.length ; i++){
			if (managers[i].id != manager_id) {
				for (var j=0 ; j<managers[i].branches.length ; j++){
					if (managers[i].branches[j].id == branch_id){
						delete managers[i].branches.splice(j, 1)
            managerFlag = true
						Manager.update({id: managers[i].id}, {branches: managers[i].branches}, function(err, manager) {
							if (err) {
								return res.view('common/error', {
									code: 'E_REM_OLD_MANAGER_UPDATE',
									message: res.i18n('E_REM_OLD_MANAGER_UPDATE'),
									details: err
								})
							} else {
								var response = ''
                if (op == 'edt'){
                  response = res.i18n('BRANCH_UPDATE')
                } else {
                  response = res.i18n('BRANCH_REMOVE')
                }
                return res.view('common/success', {
                  message: response,
                  data: branch
                })
							}

						})
					}
				}
			}
		}
    if (!managerFlag){
      return res.view('common/success', {
        message: res.i18n('BRANCH_UPDATE'),
        data: branch
      })
    }
	})
}


module.exports = {

	/**
	* @TODO 		:: Check for Authentication
	*						:: Get longititude and latitude from Google Maps API after integrating maps to the project
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	addBranch: function(req, res) {
		var name = req.param('name') ? req.param('name') : null
		var address = req.param('address') ? req.param('address') : null
		var current_manager_id = req.param('current_manager') ? req.param('current_manager') : null
		var longtitude = req.param('longtitude') ? req.param('longtitude') : null
		var latitude = req.param('latitude') ? req.param('latitude') : null
		var deleted = false

		if (req.isAuthenticated()) {
			if ( name || current_manager_id && (address && longtitude && latitude) ) {

				//Get the manager name from its own collection
				Manager.find({id: current_manager_id, deleted: false}, function(err, manager){
					if (err) {
						return res.view('common/error', {
							code: 'E_ADD_BRANCH_FIND_MANAGER',
							message: res.i18n('E_ADD_BRANCH_FIND_MANAGER'),
							details: err
						})
					}
					var current_manager = 'NONE';
					if (manager.length > 0)
						current_manager = manager[0].name;

					var branchModel = {
						name: name,
						address: address,
						current_manager: current_manager,
						current_manager_id: current_manager_id ? current_manager_id : 0,
						longtitude: longtitude,
						latitude: latitude,
						deleted: deleted
					}
					if (current_manager != 'NONE') {
						//branches owned by the current manager
						var manager_branches = manager[0].branches
						// First add the branch
						Branch.create(branchModel, function (err, branch) {
							if (err) {
								return res.view('common/error', {
									code: 'E_ADD_BRANCH_CREATE',
									message: res.i18n('E_ADD_BRANCH_CREATE'),
									details: err
								})
							}
							//Get its ID
							var new_branch = {
								name: branch.name,
								id: branch.id
							}
							//add the new branch to the manager's branches
							manager_branches.push(new_branch)
							Manager.update({id: current_manager_id}, {branches: manager_branches}, function(err, manager) {
								if (err) {
									return res.view('common/error', {
										code: 'E_ADD_BRANCH_UPDATE_MANAGER_NOT_NONE',
										message: res.i18n('E_ADD_BRANCH_UPDATE_MANAGER_NOT_NONE'),
										details: err
									})
								}
								return res.view('common/success', {
									message: res.i18n('BRANCH_ADD'),
									data: branch
								})
							})
						})
					} else {
						Branch.create(branchModel, function (err, branch) {
							if (err) {
								return res.view('common/error', {
									code: 'E_ADD_BRANCH_CREATE_MANAGER_NONE',
									message: res.i18n('E_ADD_BRANCH_CREATE'),
									details: err
								})
							}
							return res.view('common/success', {
								message: res.i18n('BRANCH_ADD'),
								data: branch
							})
						})
					}
				})
			} else {
				return res.view('common/error', {
					code: 'E_ADD_BRANCH_MISSIN_PARAMS',
					message: res.i18n('E_ADD_BRANCH_MISSIN_PARAMS'),
					details: ''
				})
			}
		} else {
			return res.redirect('/login')
		}
	},

	/**
	* @TODO 		:: Check for Authentication
	*						:: Get longititude and latitude from Google Maps API after integrating maps to the project
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	editBranch: function(req, res) {
		var id = req.param('id') ? req.param('id') : null
		var name = req.param('name') ? req.param('name') : null
		var address = req.param('address') ? req.param('address') : null
		var current_manager_id = req.param('current_manager') ? req.param('current_manager') : null
		var longtitude = req.param('longtitude') ? req.param('longtitude') : null
		var latitude = req.param('latitude') ? req.param('latitude') : null

		if (req.isAuthenticated()) {
			if ( id && name && address && current_manager_id && longtitude && latitude ) {

				//Get the manager name from its own collection
				Manager.find({id: current_manager_id, deleted: false}, function(err, manager){
					if (err) {
						return res.view('common/error', {
							code: 'E_EDIT_BRANCH_FIND_MANAGER',
							message: res.i18n('E_ADD_BRANCH_FIND_MANAGER'),
							details: err
						})
					}
					var current_manager = res.i18n('Not Found');
					if (manager.length > 0)
						current_manager = manager[0].name

					var branchModel = {
						name: name,
						address: address,
						current_manager: current_manager,
						current_manager_id: current_manager_id,
						longtitude: longtitude,
						latitude: latitude
					}

					if (current_manager != res.i18n('Not Found')) {
						// branches owned by the current manager
						var manager_branches = manager[0].branches
						// Get its ID
						var new_branch = {
							name: name,
							id: id
						}
						// Add the new branch to the manager's branches
						// Check if this branch is already owned by the manager
						var flag = true;
						for (var i=0 ; i<manager_branches.length ; i++) {
							if (manager_branches[i].id == id) {
								flag = false;
								break;
							}
						}
						if (manager_branches.length == 0){
							manager_branches = [];
						}
						if (flag) {
							// Branch can be added
							manager_branches.push(new_branch)
						}
						Manager.update({id: current_manager_id}, {branches: manager_branches}, function(err, manager) {
							if (err) {
								return res.view('common/error', {
									code: 'E_EDIT_BRANCH_WITH_CURRENT_MANAGER',
									message: res.i18n('E_ADD_BRANCH_UPDATE_MANAGER_NOT_NONE'),
									details: err
								})
							}
							Branch.update({ id: id }, branchModel, function (err, branch) {
								if (err) {
									return res.view('common/error', {
										code: 'E_EDIT_BRANCH_UPDATE',
										message: res.i18n('E_EDIT_BRANCH_UPDATE'),
										details: err
									})
								}
								removeOldManager(req, res, id, current_manager_id, branch, 'edt')
							})
						})
					} else {
						Branch.update({ id: id }, branchModel, function (err, branch) {
							if (err) {
								return res.view('common/error', {
									code: 'E_EDIT_BRANCH_UPDATE_WITH_NO_MANAGER',
									message: res.i18n('E_EDIT_BRANCH_UPDATE'),
									details: err
								})
							}
							return res.view('common/success', {
								message: res.i18n('BRANCH_UPDATE'),
								data: branch
							})
						})
					}
				})
			} else {
				return res.view('common/error', {
					code: 'E_EDIT_BRANCH_MISSING_PARAMS',
					message: 'Error occurred, No parameters where sent',
					details: ''
				})
			}
		} else {
			return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	removeBranch: function(req, res) {
		var id = req.param('id') ? req.param('id') : null

		if (req.isAuthenticated()) {
			if (id) {
				Branch.update({ id: id }, { deleted: true, current_manager: res.i18n('Not Found'), current_manager_id: res.i18n('Not Found') }, function (err, branch) {
					if (err) {
						return res.view('common/error', {
							code: 'E_REMOVE_BRANCH_UPDATE',
							message: res.i18n('E_REMOVE_BRANCH_UPDATE'),
							details: err
						})
					}
					removeOldManager(req, res, id, -999, 'del')
				})
			} else {
				return res.view('common/error', {
					code: 'E_REMOVE_BRANCH_MISSING_PARAMS',
					message: res.i18n('E_ADD_BRANCH_MISSIN_PARAMS'),
					details: err
				})
			}
		} else {
			return res.redirect('/login')
		}


	},

	/**
	* @description 		:: API FUNCTION :: return all system branches
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	getBranches: function(req, res) {
		if (req.isAuthenticated()) {
			Branch.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
						code: 'E_GET_BRANCHS',
						message: res.i18n('E_GET_BRANCHS'),
						details: err
					})
				}
				return res.json({
					message: res.i18n('GET_BRANCHES'),
					data: data
				})
			})
		} else {
			return res.redirect('/login')
		}
	},

	/**
	* @TODO 					:: Implementation & Integrating Maps API
	* @description 		:: return a list of all branches near certain Long and Lat
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	getNearestBranches: function(req, res) {
		if (req.isAuthenticated()) {
			Branch.find({deleted: false}, function (err, data) {
				if (err) {
					return res.json({
						message: 'Error occurred while retrieving branches: ' + err,
						data: false
					})
				}
				return res.json({
					message: 'Branches are all retrieved',
					data: data
				})
			})
		} else {
			return res.redirect('/login')
		}
	},

	/**
	* @description 		:: return all system branches for the branches page
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	branchesPage: function(req, res) {
		if (req.isAuthenticated()) {
			Branch.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
						code: 'E_BRANCHES_PAGE',
						message: res.i18n('E_GET_BRANCHS'),
						details: err
					})
				}
				return res.view('branch/branches', {
					message: res.i18n('GET_BRANCHES'),
					data: data
				})
			})
		} else {
			return res.redirect('/login')
		}
	},

	/**
	* @description 		:: return system branch by ID for editing it, and system Managers to select a manager from them
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	getBranchByID_forEdit: function(req, res) {
		var id = req.param('id') ? req.param('id') : null

		if (req.isAuthenticated()) {
			if (id) {
				Branch.find({id: id, deleted: false}, function (err, data) {
					if (err) {
						return res.view('common/error', {
							code: 'E_BRANCHES_EDIT_PAGE',
							message: res.i18n('E_GET_BRANCHS'),
							details: err
						})
					}
					Manager.find({deleted: false}, function (err, managers) {
						if (err) {
							return res.view('common/error', {
								code: 'E_BRANCHES_EDIT_PAGE_MANAGER_FIND',
								message: res.i18n('E_BRANCHES_EDIT_PAGE_MANAGER_FIND'),
								details: err
							})
						}
						return res.view('branch/editBranch', {
							message: res.i18n('GET_MANAGERS'),
							data: data[0],
							managers: managers
						})
					})
				})
			} else {
				return res.view('common/error', {
					code: 'E_BRANCHES_EDIT_PAGE_MISSING_PARAMS',
					message: res.i18n('E_ADD_BRANCH_MISSIN_PARAMS'),
					details: err
				})
			}
		} else {
			return res.redirect('/login')
		}



	},

	/**
	* @description 		:: return managers to the add page
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	addBranchPage: function(req, res) {

		if (req.isAuthenticated()) {
			Manager.find({deleted: false}, function (err, managers) {
				if (err) {
					return res.view('common/error', {
						code: 'E_BRANCH_ADD_PAGE_FIND_MANAGER',
						message: res.i18n('E_BRANCHES_EDIT_PAGE_MANAGER_FIND'),
						details: err
					})
				}
				return res.view('branch/addBranch', {
					message: res.i18n('GET_MANAGERS'),
					managers: managers
				})
			})
		} else {
			return res.redirect('/login')
		}


	},


};
