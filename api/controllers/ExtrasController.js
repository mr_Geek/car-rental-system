/**
 * ExtrasController
 *
 * @description :: Server-side logic for managing extras
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

		/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	addExtras: function(req, res) {

		if (req.isAuthenticated()) {
			var name = req.param('name') ? req.param('name') : ''
			var price = req.param('price') ? req.param('price') : ''

			if ( name || price ) {
				var extrasModel = {
					name: name,
					price: price,
				}
				Extras.create(extrasModel, function(err, extras) {
					if(err) {
						return res.view('common/error', {
						  code: 'E_EXTRAS_CREATE',
						  message: res.i18n('E_EXTRAS_CREATE'),
						  details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('ADD_EXTRAS'),
						data: extras
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_EXTRAS_CREATE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	editExtras: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var name = req.param('name') ? req.param('name') : null
			var price = req.param('price') ? req.param('price') : null

			if ( id && name && price ) {
				var extrasModel = {
					name: name,
					price: price,
				}
				Extras.update({ id: id }, extrasModel, function(err, extras) {
					if(err) {
						return res.view('common/error', {
						  code: 'E_EXTRAS_EDIT',
						  message: res.i18n('E_EXTRAS_EDIT'),
						  details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('UPDATE_EXTRAS'),
						data: extras
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_EXTRAS_EDIT_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	removeExras: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Extras.update({ id: id }, { deleted: true }, function (err, extras) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_EXTRAS_REMOVE',
						  message: res.i18n('E_EXTRAS_REMOVE'),
						  details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('REMOVE_EXTRAS'),
						data: extras
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_EXTRAS_REMOVE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})

			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: API FUNCTION :: return all system extras
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	getExtras: function(req, res) {

		if (req.isAuthenticated()) {
			Extras.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_GET_EXTRAS',
					  message: 'Error occurred while retrieving extras',
					  details: err
					})
				}
				return res.json({
					message: 'Extras are all retrieved',
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return all system extras to the page
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	extrasPage: function(req, res) {
		if (req.isAuthenticated()) {
			Extras.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_GET_EXTRAS_PAGE',
					  message: res.i18n('E_GET_EXTRAS_PAGE'),
					  details: err
					})
				}
				return res.view('extras/extras', {
					message: res.i18n('GET_EXTRAS'),
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return all system extras
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	editExtrasPage: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Extras.find({ id: id, deleted: false }, function (err, extras) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_EDIT_EXTRAS_PAGE',
						  message: res.i18n('E_ADD_CAR_GET_EXTRAS'),
						  details: err
						})
					}
					return res.view('extras/editExtras', {
						message: res.i18n('GET_EXTRAS'),
						data: extras[0]
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_EDIT_EXTRAS_PAGE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},



};
