/**
 * CommonController
 *
 * @description :: Server-side logic for managing commons
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	errorPage: function(req, res) {
		if (req.isAuthenticated()) {
			return res.view('common/error')
		} else {
			return res.redirect('/login')
		}
	},

	successPage: function(req, res) {
		if (req.isAuthenticated()) {
			return res.view('common/success')
		} else {
			return res.redirect('/login')
		}
	},

	homePage: function(req, res) {
		if (req.isAuthenticated()) {
			return res.view('homepage')
		} else {
			return res.redirect('/login')
		}
	},
};
