/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport')


/**
* @description 		:: API FUNCTION :: check for authentication
* @return {JSON} {
*										message,
*										data => (false) if error occurred
*								 }
*/
function _onPassportAuth(req, res, err, user, info) {
	if (err) {
		return res.serverError(err)
	}
	if (!user) {
		return res.unauthorized(null, info && info.code, info && info.message)
	}

	req.logIn(user, function(err){
		if (err) {
			console.log("+ Auth.Login err ", err);
			res.send(err);
		}
		if (info.type === 'manager') {
			req.session.me = true
			req.session.userId = user.id
      req.session.user = user.username
      req.session.admin = user.admin
		} else {
			req.session.userId = ''
      req.session.me = ''
      req.session.user = ''
      req.session.admin = ''
    }
		return res.ok({
			code: 'S_LOGIN_SUCCESS',
			message: 'User is logged',
			token: CipherService.createToken(user),
			data: user
		})
	});
}

function _onPassportSystemAuth(req, res, err, user, info) {
	if (err) {
		return res.view('common/error', {
			code: 'E_AUTHENTICATION',
			message: 'Error occurred while login',
			details: err
		})
	}
	if (!user) {
		return res.unauthorized(null, info && info.code, info && info.message)
	}

	req.logIn(user, function(err){
		if (err) {
			return res.view('common/error', {
				code: 'E_LOGIN',
				message: res.i18n('E_LOGIN'),
				details: err
			})
		}
		if (info.type === 'manager') {
			req.session.me = true
			req.session.userId = user.id
      req.session.user = user.username
      req.session.admin = user.admin
		} else {
			req.session.userId = ''
      req.session.me = ''
      req.session.user = ''
      req.session.admin = ''
    }
		req.session.notifications = 0
		return res.redirect('/')
	});
}


module.exports = {

	loginPage: function(req, res) {
		if (req.isAuthenticated()) {
			res.redirect('/')
		} else {
			res.view('auth/login')
		}
	},

	registerPage: function(req, res) {
		if (req.isAuthenticated()){
			res.view('auth/signup');
		}
		else{
			res.badRequest();
		}
	},

	loginProcess: function(req, res) {
		var formUserName = req.param("username");
		var formPassword = req.param("password");

		if (formUserName && formPassword){
			console.log("+ Auth.Login username=", formUserName, " and form password=", formPassword);
			passport.authenticate('local', _onPassportAuth.bind(this, req, res))(req, res);
		}
		else{
			return res.json({
				code: 'E_LOGIN_EMPTY_CRED',
				message: res.i18n('E_LOGIN_EMPTY_CRED'),
				data: false
			})
		}
	},

	loginSystemProcess: function(req, res){
		var formUserName = req.param("username");
		var formPassword = req.param("password");

		if (formUserName && formPassword){
			passport.authenticate('local', _onPassportSystemAuth.bind(this, req, res))(req, res);
		}
		else{
			return res.redirect('/login')
		}
	},

	registerProcess: function(req, res){
			var username = req.param('username') ? req.param('username') : null
			var password = req.param('password') ? req.param('password') : null
			var name = req.param('name') ? req.param('name') : null
			var id_num = req.param('id_num') ? req.param('id_num') : null
			var phone = req.param('phone') ? req.param('phone') : null
			var address = req.param('address') ? req.param('address') : null
			var lng = req.param('lng') ? req.param('lng') : null
			var lat = req.param('lat') ? req.param('lat') : null
			var email = req.param('email') ? req.param('email') : null

			if (username && password && name && id_num && phone && address && lng && lat && email ) {
				var userModel = {
					provider: "local",
					username: username,
					password: password,
					name: name,
					id_num: id_num,
					phone: phone,
					address: address,
					lng: lng,
					lat: lat,
					email: email
				};

				User.findOne({username: username}).exec(function(err, user){
					if (err){
						return res.json({
							code: 'E_REGISTER_FIND_DB',
							token: false,
							message: 'Error while finding user' + err,
							data: false
						})
					}
					if (!user){
						User.create(userModel, function(err, user){
							if (err) {
								return res.json({
									code: 'E_REGISTER_CREATE_DB',
									token: false,
									message: 'Error while creating new user' + err,
									data: false
								})
							}
							else {
								return res.json({
									code: 'S_REGISTER_SUCCESS',
									message: 'New user has been added to the DB',
									token: CipherService.createToken(user),
									data: user
								})
							}
						})
					}
					else{
						return res.json({
							code: 'E_ALREADY_REGISTERD',
							message: 'User already exists',
							token: false,
							data: user
						})
					}
				});
			}
			else {
				return res.json({
					code: 'E_REGISTER_EMPTY_CRED',
					token: false,
					message: 'Error there are missing credentials',
					data: false
				})
			}
		// } else {
		// 	return res.badRequest();
		// }
	},

	logout: function(req, res){
		console.log("+ Auth.Logout ");
		req.session.me = false;
		req.session.user = null;
		req.session.notifications = 0
		req.logOut();
		return res.redirect('/');
	}


};
