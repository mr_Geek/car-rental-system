/**
 * ManagerController
 *
 * @description :: Server-side logic for managing managers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

function checkRelevant(req, res, manager_id, actualBranches, manager, op) {
	/* Check if there are any branches should be nulled */
	Branch.find({deleted: false}, function(err, branches) {
		if (err) {
			return res.view('common/error', {
			  code: 'E_CHECK_REL_BRANCH_FIND',
			  message: res.i18n('E_CHECK_REL_BRANCH_FIND'),
			  details: err
			})
		}

		var toBeUpdatedBranchesIDs = []

		for(var i=0 ; i<branches.length ; i++) {
			/* Check if there is a branch we manage */
			if (branches[i].current_manager_id == manager_id) {
				// /* flag to check if the branch is not in the list or not */
				var flag = false
				for (var j=0 ; j<actualBranches.length ; j++) {
					if (actualBranches[j].id == branches[i].id) {
						flag = true
						break
					}
				}
				if (!flag) {
					toBeUpdatedBranchesIDs.push(branches[i].id)
				}
			}
		}

		Branch.update({id: toBeUpdatedBranchesIDs}, {current_manager: res.i18n('Not Found'), current_manager_id: res.i18n('Not Found')}, function(err, branches) {
			if (err) {
				return res.view('common/error', {
				  code: 'E_CHECK_REL_BRANCH_UPDATE',
				  message: res.i18n('E_CHECK_REL_BRANCH_UPDATE'),
				  details: err
				})
			}
			var response = ''
			if (op == 'del'){
				response = res.i18n('REMOVE_MANAGER')
			} else {
				response = res.i18n('UPDATE_MANAGER')
			}

			return res.view('common/success', {
				message: response,
				data: manager
			})
		})
	})
}


module.exports = {

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	addManager: function(req, res) {

		if (req.isAuthenticated()) {
			var name = req.param('name') ? req.param('name') : null
			var username = req.param('username') ? req.param('username') : null
			var password = req.param('password') ? req.param('password') : null
			var branches = req.param('branches') ? [].concat(req.param('branches')) : []

			var actualBranches = []
			var branches_ids = []
			for (var i=0 ; i<branches.length ; i++) {
				var obj = {
					id: branches[i].split('|')[0],
					name: branches[i].split('|')[1]
				}
				actualBranches.push(obj)
				branches_ids.push(branches[i].split('|')[0])
			}
			if ( name && username && password && branches ) {
				var managerModel = {
					name: name,
					username: username,
					password: password,
					branches: actualBranches,
	        deleted: false
				}
				Manager.create(managerModel, function(err, manager) {
					if(err) {
						return res.view('common/error', {
						  code: 'E_ADD_MANAGER',
						  message: res.i18n('E_ADD_MANAGER'),
						  details: err
						})
					}
					Branch.update({id: branches_ids}, {current_manager: manager.name, current_manager_id: manager.id}, function(err, branches) {
						if (err) {
							return res.view('common/error', {
							  code: 'E_ADD_MANAGER_UPDATE',
							  message: res.i18n('E_CHECK_REL_BRANCH_UPDATE'),
							  details: err
							})
						}
						return res.view('common/success', {
							message: res.i18n('ADD_MANAGER'),
							data: manager
						})
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_ADD_MANAGER_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
			return res.redirect('/login')
		}

	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	editManager: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var name = req.param('name') ? req.param('name') : null
			var username = req.param('username') ? req.param('username') : null
			var password = req.param('password') ? req.param('password') : null
			var branches = req.param('branches') ? [].concat(req.param('branches')) : []

			var actualBranches = []
			var branches_ids = []
			for (var i=0 ; i<branches.length ; i++) {
				var obj = {
					id: branches[i].split('|')[0],
					name: branches[i].split('|')[1]
				}
				actualBranches.push(obj)
				branches_ids.push(branches[i].split('|')[0])
			}
			if (id && name && username && password && branches) {
				var managerModel = {
					name: name,
					username: username,
					password: password,
					branches: actualBranches
				}
				Manager.update({ id: id }, managerModel, function(err, manager) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_EDIT_MANAGER_UPDATE_MANAGER',
						  message: res.i18n('E_EDIT_MANAGER_UPDATE_MANAGER'),
						  details: err
						})
					}
					Branch.update({id: branches_ids}, {current_manager: name, current_manager_id: id}, function(err, branches) {
						if (err) {
							return res.view('common/error', {
							  code: 'E_EDIT_MANAGER_UPDATE_BRANCH',
							  message: res.i18n('E_EDIT_BRANCH_UPDATE'),
							  details: err
							})
						}

						checkRelevant(req, res, id, actualBranches, manager, 'edt')

					})


				})
			} else {
				return res.view('common/error', {
				  code: 'E_EDIT_MANAGER_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
			return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	removeManager: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Manager.update({ id: id }, { deleted: true, branches: [] }, function (err, manager) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_REM_MANAGER_UPDATE_MANAGER',
						  message: res.i18n('E_REM_MANAGER_UPDATE_MANAGER'),
						  details: err
						})
					}
					checkRelevant(req, res, id, [], manager, 'del')
				})
			} else {
				return res.view('common/error', {
				  code: 'E_REM_MANAGER_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
			return res.redirect('/login')
		}


	},

	/**
	* @description 		:: API FUNCTION :: return all system managers
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	getManagers: function(req, res) {
		if (req.isAuthenticated()) {
			Manager.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_GET_MANAGERS',
					  message: 'Error occurred while retrieving managers',
					  details: err
					})
				}
				return res.json({
					message: 'Managers are all retrieved',
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}
	},

	/**
	* @description 		:: return all system managers
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	managersPage: function(req, res) {
		if (req.isAuthenticated()) {
			Manager.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_MANAGERS_PAGE',
					  message: res.i18n('E_MANAGERS_PAGE'),
					  details: err
					})
				}
				return res.view('manager/managers', {
					message: res.i18n('GET_MANAGERS'),
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}

	},

	/**
	* @description 		:: return system manager by ID for editing it, and system Branches to select a branch from them
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	getManagerByID_forEdit: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Manager.find({id: id}, function (err, data) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_MANAGERS_EDIT_PAGE',
						  message: res.i18n('E_MANAGERS_PAGE'),
						  details: err
						})
					}
					Branch.find({current_manager: res.i18n('Not Found'), deleted: false}, function (err, branches) {
						if (err) {
							return res.view('common/error', {
							  code: 'E_MANAGERS_EDIT_PAGE_FIND_BRANCHES',
							  message: res.i18n('E_GET_BRANCHS'),
							  details: err
							})
						}
						return res.view('manager/editManager', {
							message: res.i18n('GET_MANAGERS'),
							data: data[0],
							branches: branches
						})
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_MANAGER_EDIT_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}
	},

	/**
	* @description 		:: return branches to the add page
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	addManagerPage: function(req, res) {

		if (req.isAuthenticated()) {
			Branch.find({current_manager: res.i18n('Not Found'), deleted: false}, function (err, branches) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_ADD_MANAGER_PAGE',
					  message: res.i18n('E_GET_BRANCHS'),
					  details: err
					})
				}
				return res.view('manager/addManager', {
					message: res.i18n('GET_BRANCHES'),
					branches: branches
				})
			})
		} else {
		  return res.redirect('/login')
		}

	},

};
