/**
 * CarController
 *
 * @description :: Server-side logic for managing Cars
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data =>  (false) if error occurred
	*								 }
	*/
	addCar: function(req, res) {

		if (req.isAuthenticated()) {
			var type = req.param('type') ? req.param('type') : ''
			var brand = req.param('brand') ? req.param('brand') : ''
			var model = req.param('model') ? req.param('model') : ''
			var year = req.param('year') ? req.param('year') : ''
			var ac = req.param('ac') ? req.param('ac') : false
			var auto_trans = req.param('auto_trans') ? req.param('auto_trans') : ''
			var number_of_doors = req.param('number_of_doors') ? req.param('number_of_doors') : '4'
			var fuel_type = req.param('fuel_type') ? req.param('fuel_type') : ''
			var price_per_day = req.param('price_per_day') ? req.param('price_per_day') : ''
			var max_per_day = req.param('max_per_day') ? req.param('max_per_day') : ''
			var extra_price = req.param('extra_price') ? req.param('extra_price') : ''
			var extra_options = req.param('extra_options') ? [].concat(req.param('extra_options')) : []

			var actualExtras = []
			for (var i=0 ; i<extra_options.length ; i++) {
				var obj = {
					id: extra_options[i].split('|')[0],
					name: extra_options[i].split('|')[1]
				}
				actualExtras.push(obj)
			}

			if ( type || brand || model || year || ac || auto_trans || number_of_doors ||
				fuel_type || price_per_day || max_per_day  || extra_price || extra_options ) {
				var carModel = {
					type: type,
					brand: brand,
					model: model,
					year: year,
					ac: ac,
					auto_trans: auto_trans,
					number_of_doors: number_of_doors,
					fuel_type: fuel_type,
					price_per_day: price_per_day,
					max_per_day: max_per_day,
					extra_price: extra_price,
					extra_options: extra_options,
				}
				Car.create(carModel, function(err, car) {
					if(err) {
						return res.view('common/error', {
							code: 'E_CAR_CREATE',
							message: res.i18n('E_CAR_CREATE'),
							details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('CAR_ADD'),
						data: car
					})
				})
			} else {
				return res.view('common/error', {
					code: 'E_CAR_CREATE_MISSING_PARAMS',
					message: res.i18n('E_MISSIN_PARAMS'),
					details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	editCar: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var type = req.param('type') ? req.param('type') : null
			var brand = req.param('brand') ? req.param('brand') : null
			var model = req.param('model') ? req.param('model') : null
			var year = req.param('year') ? req.param('year') : null
			var ac = req.param('ac') ? req.param('ac') : null
			var auto_trans = req.param('auto_trans') ? req.param('auto_trans') : null
			var number_of_doors = req.param('number_of_doors') ? req.param('number_of_doors') : null
			var fuel_type = req.param('fuel_type') ? req.param('fuel_type') : null
			var price_per_day = req.param('price_per_day') ? req.param('price_per_day') : null
			var max_per_day = req.param('max_per_day') ? req.param('max_per_day') : null
			var extra_price = req.param('extra_price') ? req.param('extra_price') : null
			var extra_options = req.param('extra_options') ? [].concat(req.param('extra_options')) : []

			var actualExtras = []
			for (var i=0 ; i<extra_options.length ; i++) {
				var obj = {
					id: extra_options[i].split('|')[0],
					name: extra_options[i].split('|')[1]
				}
				actualExtras.push(obj)
			}

			if ( id && type && brand && model && year && ac && auto_trans && number_of_doors &&
				fuel_type && price_per_day && max_per_day && extra_price && extra_options ) {
				var carModel = {
					type: type,
					brand: brand,
					model: model,
					year: year,
					ac: ac,
					auto_trans: auto_trans,
					number_of_doors: number_of_doors,
					fuel_type: fuel_type,
					price_per_day: price_per_day,
					max_per_day: max_per_day,
					extra_price: extra_price,
					extra_options: actualExtras,
				}
				Car.update({ id: id }, carModel, function(err, car) {
					if(err) {
						return res.view('common/error', {
							code: 'E_CAR_UPDATE',
							message: res.i18n('E_CAR_UPDATE'),
							details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('CAR_UPDATE'),
						data: car
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_CAR_UPDATE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	removeCar: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Car.update({ id: id }, { deleted: true }, function (err, car) {
					if (err) {
						return res.view('common/error', {
							code: 'E_CAR_REMOVE',
							message: res.i18n('E_CAR_REMOVE'),
							details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('CAR_REMOVE'),
						data: car
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_CAR_REMOVE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: API FUNCTION :: return all system cars
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	getCars: function(req, res) {
		Car.find({deleted: false}, function (err, data) {
			if (err) {
				return res.json({
					code: 'E_RET_CARS',
					message: 'Error occurred while retrieving cars: ' + err,
					data: false
				})
			}
			return res.json({
				code: 'S_RET_CARS',
				message: 'Cars are all retrieved.',
				data: data
			})
		})

	},

	/**
	* @description 		:: return all system cars to cars page
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	carsPage: function(req, res) {

		if (req.isAuthenticated()) {
			Car.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_GET_CARS',
					  message: res.i18n('E_GET_CARS'),
					  details: err
					})
				}
				return res.view('car/cars', {
					message: res.i18n('GET_CARS'),
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return extras to the car add page
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	addCarPage: function(req, res) {

		if (req.isAuthenticated()) {
			Extras.find({deleted: false}, function (err, extras) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_ADD_CAR_GET_EXTRAS',
					  message: res.i18n('E_ADD_CAR_GET_EXTRAS'),
					  details: err
					})
				}
				return res.view('car/addCar', {
					message: res.i18n('GET_EXTRAS'),
					extras: extras
				})
			})
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return system Car by ID for editing it, and system Extras to select a branch from them
	* @return {JSON} {
	*										message,
	*										data => (false) if error occurred
	*								 }
	*/
	getCarByID_forEdit: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Car.find({id: id}, function (err, data) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_EDIT_CAR_FIND_CAR',
						  message: res.i18n('E_EDIT_CAR_FIND_CAR'),
						  details: err
						})
					}
					Extras.find({deleted: false}, function (err, extras) {
						if (err) {
							return res.view('common/error', {
							  code: 'E_EDIT_CAR_FIND_EXTRAS',
							  message: res.i18n('E_ADD_CAR_GET_EXTRAS'),
							  details: err
							})
						}
						return res.view('car/editCar', {
							message: res.i18n('GET_EXTRAS'),
							data: data[0],
							extras: extras
						})
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_CAR_EDIT_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})

			}
		} else {
		  return res.redirect('/login')
		}



	},



};
