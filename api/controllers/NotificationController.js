/**
 * NotificationController
 *
 * @description :: Server-side logic for managing notifications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var jwt = require('jsonwebtoken')

module.exports = {

	/**
	* @TODO 		:: API FUNCTION :: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	addNotification: function(req, res) {
    var authorization = req.headers.authorization ? req.headers.authorization : null
		if(authorization) {
			try{
	      jwt.verify(authorization, sails.config.jwtSettings.secret, function(err, decoded) {

					var client_name = decoded.user.name ? decoded.user.name : null
					var phone_number = decoded.user.phone ? decoded.user.phone : null
					var id_number = decoded.user.id_num ? decoded.user.id_num : null
					var email = decoded.user.email ? decoded.user.email : null
					var address = decoded.user.address ? decoded.user.address : ''
					var longtitude = decoded.user.lng ? decoded.user.lng : ''
					var latitude = decoded.user.lat ? decoded.user.lat : ''
					var car_id = req.param('car_id') ? req.param('car_id') : null

					if (client_name && phone_number && id_number && car_id && (email || address || longtitude || latitude)) {
						Car.findOne({id: car_id}, function(err, car) {
							if(err) {
								return res.json({
									code: 'E_ADD_NOTIFICATION_FETCH_CAR',
									message: 'Error occurred while fetching car info: ' + err,
									data: false
								})
							}
							var notificationModel = {
								client_name: client_name,
								phone_number: phone_number,
								id_number: id_number,
								email: email,
								address: address,
								longtitude: longtitude,
								latitude: latitude,
								car_id: car.id,
								car_name: car.brand
							}
							Notification.create(notificationModel, function(err, notification) {
								if(err) {
									return res.json({
										code: 'E_ADD_NOTIFICATION',
										message: 'Error occurred while creating new notification: ' + err,
										data: false
									})
								}
								Notification.publishCreate({
									id: notification.id,
									message: notification.client_name + 'Has ordered new Car'
								})
								return res.json({
									code: 'S_ADD_NOTIFICATION',
									message: 'Notification created successfully',
									data: notification
								})
							})
						})
					} else {
						return res.json({
							code: 'E_ADD_NOTIFICATION_EMPTY_CRED',
							message: 'Error there are missing credentials',
							data: false
						})
					}
	      })
	    } catch (err) {
	      console.log(err)
				return res.json({
					code: 'E_ADD_NOTIFICATION_NOT_AUTHORIZED',
					message: 'Error occurred, Probably not authorized.',
					data: false
				})
	    }
		} else {
			return res.json({
				code: 'E_ADD_NOTIFICATION_NOT_AUTHORIZED',
				message: 'Error occurred, Probably not authorized.',
				data: false
			})
		}
	},

	/**
	* @TODO 		:: Check for Authentication
	*						:: CHECK FOR MANAGERS COLLISION
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	editNotification: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var status = req.param('status') ? req.param('status') : null
			var status_detils = req.param('status_detils') ? req.param('status_detils') : null
			var manager_name = req.param('manager_name') ? req.param('manager_name') : '  '
			var manager_id = req.param('manager_id') ? req.param('manager_id') : '  '

			if (id && status && status_detils  && manager_name && manager_id) {
				var notificationModel = {
					status: status,
					status_detils: status_detils,
					manager_name: manager_name,
					manager_id: manager_id,
				}
				Notification.update({ id: id }, notificationModel, function(err, notification) {
					if(err) {
						return res.view('common/error', {
							code: 'E_EDIT_NOTIFICATION',
							message: res.i18n('E_EDIT_NOTIFICATION'),
							details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('UPDATE_NOTIF'),
						data: notification
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_NOTIF_EDIT_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @TODO 		:: Check for Authentication
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	removeNotification: function(req, res) {

		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Notification.update({ id: id }, { deleted: true }, function (err, notification) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_REMOVE_NOTIFICATION',
						  message: res.i18n('E_REMOVE_NOTIFICATION'),
						  details: err
						})
					}
					return res.view('common/success', {
						message: res.i18n('REMOVE_NOTIF'),
						data: notification
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_NOTIF_REMOVE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return all system notifications
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	getNotifications: function(req, res) {

		if (req.isAuthenticated()) {
			Notification.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_GET_NOTIFICATION',
					  message: res.i18n('E_GET_NOTIFICATION'),
					  details: err
					})
				}
				req.session.notifications = 0
				return res.view('common/success', {
					message: res.i18n('GET_NOTIF'),
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return all system notifications
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	notificationsPage: function(req, res) {

		if (req.isAuthenticated()) {
			Notification.find({deleted: false}, function (err, data) {
				if (err) {
					return res.view('common/error', {
					  code: 'E_NOTIF_PAGE_FIND_NOTIFS',
					  message: res.i18n('E_GET_NOTIFICATION'),
					  details: err
					})
				}
				req.session.notifications = 0
				return res.view('notification/notifications', {
					message: res.i18n('GET_NOTIF'),
					data: data
				})
			})
		} else {
		  return res.redirect('/login')
		}


	},

	/**
	* @description 		:: return system notification by ID for editing it
	* @return {JSON} {
	*										message,
	*										data => (false) if error occured
	*								 }
	*/
	getNotificationByID_forEdit: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			if (id) {
				Notification.find({id: id, deleted: false}, function (err, data) {
					if (err) {
						return res.view('common/error', {
						  code: 'E_NOTIF_EDIT_PAGE_FIND_NOTIFS',
						  message: res.i18n('E_GET_NOTIFICATION'),
						  details: err
						})
					}
					Car.find({deleted: false, id: data[0].car_id}, function(err, car) {
						if (err) {
							return res.view('common/error', {
							  code: 'E_NOTIF_EDIT_PAGE_CAR_FIND',
							  message: res.i18n('E_EDIT_CAR_FIND_CAR'),
							  details: err
							})
						}
						return res.view('notification/editNotification', {
							message: res.i18n('GET_NOTIF'),
							data: data[0],
							car: car[0]
						})
					})
				})
			} else {
				return res.view('common/error', {
				  code: 'E_NOTIF_EDIT_PAGE_MISSING_PARAMS',
				  message: res.i18n('E_MISSIN_PARAMS'),
				  details: ''
				})
			}
		} else {
		  return res.redirect('/login')
		}



	},

	subscribeToNotification: function(req, res) {
		if (req.isSocket) {
			//Subscribe the user to the changes
			//when connected with GET method
			Notification.watch(req.socket)
			return res.json({
				message: 'Subscribed to socket id: ' + req.socket.id
			})
		} else {
			return res.json({
				message: 'Not Socket Connection'
			})
		}
	},

	updateNoti: function(req, res) {
		if (req.isSocket) {
			//Subscribe the user to the changes
			//when connected with GET method
			req.session.notifications++
			return res.json({
				message: 'updated.',
				number: req.session.notifications
			})
		} else {
			return res.json({
				message: 'Not Socket Connection'
			})
		}
	},

};
