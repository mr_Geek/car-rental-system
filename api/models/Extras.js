/**
 * Extras.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	name: {
  		type: 'string'
  	},

  	// @TODO: Deal it as a number
  	price: {
  		type: 'string'
  	},

  	deleted: {
  		type: 'boolean',
  		defaultsTo: false
  	},

  }
};

