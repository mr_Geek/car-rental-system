/**
 * Branch.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name: {
      type: 'string'
    },

  	address: {
  		type: 'string'
  	},

  	// @TODO: Choose the current manager from a list of all managers
    // Objectid refer to the manager in managers
    // One Manager for every branch
  	current_manager: {
  		type: 'string'
  	},

    current_manager_id: {
      type: 'string'
    },

  	longtitude: {
  		type: 'string'
  	},

  	latitude: {
  		type: 'string'
  	},

    deleted: {
      type: 'boolean',
      defualtsTo: false
    }

  }
};
