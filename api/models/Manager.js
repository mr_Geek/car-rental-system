/**
 * Manager.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bycrypt = require('bcryptjs');

module.exports = {

  adapter: 'mongo',

  schema: true,

  attributes: {

  	name: {
  		type: 'string'
  	},

  	username: {
  		type: 'string'
  	},

  	password: {
  		type: 'string'
  	},

  	// One manager could be responsible for many branches
  	branches: {
  		type: 'json',
  		defaultsTo: []
  	},

  	deleted: {
      type: 'boolean',
      defualtsTo: false
    },

    admin: {
      type: 'boolean',
      defaultsTo: false
    },

    notifications: {
      type: 'string',
      defaultsTo: '0'
    },

    toJSON: function(){
      var obj = this.toObject()
      delete obj.password
      delete obj.notifications
      return obj
    }

  },

  beforeCreate: function(manager, cb) {
    bycrypt.genSalt(10, function(err, salt) {
      if (err) {
        console.log('Error generating SALT for user: ' + manager + '\n Error: ' + err);
        cb(err);
      }
      bycrypt.hash(manager.password, salt, function(err, hash) {
        if (err) {
          console.log('Error generating HASH for user: ' + manager + '\n Error: ' + err);
          cb(err);
        } else {
          manager.password = hash
          cb(null, manager)
        }
      })

    })
  }


};
