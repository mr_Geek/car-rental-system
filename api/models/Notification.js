/**
 * Notification.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	client_name: {
  		type: 'string',
      defaultsTo: ''
  	},

  	phone_number: {
  		type: 'string',
      defaultsTo: ''
  	},

  	id_number: {
  		type: 'string',
      defaultsTo: ''
  	},

  	email: {
  		type: 'string',
      defaultsTo: ''
  	},

  	// Means his location right now
  	address: {
  		type: 'string',
      defaultsTo: ''
  	},

  	longtitude: {
  		type: 'string',
      defaultsTo: ''
  	},

  	latitude: {
  		type: 'string',
      defaultsTo: ''
  	},

    car_id: {
      type: 'string',
      defaultsTo: ''
    },

  	status: {
  		type: 'string',
      defaultsTo: ''
  	},

  	status_detils: {
  		type: 'string',
      defaultsTo: ''
  	},

    manager_name: {
      type: 'string',
      defaultsTo: ''
    },

    manager_id: {
      type: 'string',
      defaultsTo: ''
    },

    read: {
      type: 'boolean',
      defaultsTo: false
    },

    deleted: {
      type: 'boolean',
      defaultsTo: false
    },



  }
};
