/**
 * Car.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	type: {
  		type: 'string'
  	},

  	brand: {
  		type: 'string'
  	},

  	model: {
  		type: 'string'
  	},

  	year: {
  		type: 'string'
  	},

  	ac: {
  		type: 'string'
  	},

  	auto_trans: {
  		type: 'string'
  	},

  	// @TODO: Deal it as a number
  	number_of_doors: {
  		type: 'string'
  	},

  	fuel_type: {
  		type: 'string'
  	},

  	// @TODO: Deal it as a number
  	price_per_day: {
  		type: 'string'
  	},

  	// @TODO: Deal it as a number
  	max_per_day: {
  		type: 'string'
  	},

  	// @TODO: Deal it as a number
  	extra_price: {
  		type: 'string'
  	},

  	extra_options: {
  		type: 'json'
  	},

    deleted: {
      type: 'boolean',
      defaultsTo: false
    },

  }
};
